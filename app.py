from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')


@app.route('/uploaded_file', methods=['POST', 'GET'])
def upload():

    if request.method == "POST":
        return render_template('home.html', start=True)

    else:
        return "error"

if __name__ == '__main__':
    app.run(debug=True)
